######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

variable "remoteproxy_count" {
  type      = number
  sensitive = false
  default   = 1
}

variable "cloudflare_default_ttl" {
  type      = number
  sensitive = false
  default   = 3600
}

variable "cloudflare_zone_id" {
  type      = string
  sensitive = true
}
