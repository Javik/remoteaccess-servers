######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

####
## Javik Remote Access Proxy
####

resource "hcloud_server" "remoteproxy" {
  count = var.remoteproxy_count
  name = format("%s-%s.%s",
    "remote-proxy${count.index + 1}",
    (count.index % 2 == 0 ? "fsn1" : "nbg1"),
    "infra.sirjavik.de"
  )

  image       = "debian-12"
  server_type = "cx11"
  location    = (count.index % 2 == 0 ? "fsn1" : "nbg1")

  labels = {
    service   = "remote-proxy"
    terraform = true
  }

  public_net {
    ipv4 = hcloud_primary_ip.remoteproxy_primary_ipv4[count.index].id
    ipv6 = hcloud_primary_ip.remoteproxy_primary_ipv6[count.index].id
  }

  ssh_keys = concat(
    [for key in hcloud_ssh_key.user_ssh : key.name],
    [hcloud_ssh_key.terraform_ssh.name]
  )

  connection {
    type        = "ssh"
    user        = "root"
    private_key = tls_private_key.terraform_ssh.private_key_openssh
    host        = self.ipv4_address
  }

  provisioner "file" {
    content     = tls_private_key.terraform_ssh.private_key_openssh
    destination = "/root/.ssh/terraform_key"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod --verbose 600 /root/.ssh/terraform_key",
    ]
  }
}
