######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

####
## Javik Remote Access Proxy
####

## DNS

resource "cloudflare_record" "remoteproxy_dns_ipv4" {
  count   = var.remoteproxy_count
  zone_id = var.cloudflare_zone_id
  name    = hcloud_server.remoteproxy[count.index].name
  value   = hcloud_primary_ip.remoteproxy_primary_ipv4[count.index].ip_address
  type    = "A"
  ttl     = var.cloudflare_default_ttl
}

resource "cloudflare_record" "remoteproxy_dns_ipv6" {
  count   = var.remoteproxy_count
  zone_id = var.cloudflare_zone_id
  name    = hcloud_server.remoteproxy[count.index].name
  value   = "${hcloud_primary_ip.remoteproxy_primary_ipv6[count.index].ip_address}1"
  type    = "AAAA"
  ttl     = var.cloudflare_default_ttl
}

## rDNS

resource "hcloud_rdns" "remoteproxy_rdns_ipv4" {
  count         = var.remoteproxy_count
  primary_ip_id = hcloud_primary_ip.remoteproxy_primary_ipv4[count.index].id
  ip_address    = hcloud_primary_ip.remoteproxy_primary_ipv4[count.index].ip_address
  dns_ptr       = hcloud_server.remoteproxy[count.index].name
}
resource "hcloud_rdns" "remoteproxy_rdns_ipv6" {
  count         = var.remoteproxy_count
  primary_ip_id = hcloud_primary_ip.remoteproxy_primary_ipv6[count.index].id
  ip_address    = hcloud_primary_ip.remoteproxy_primary_ipv6[count.index].ip_address
  dns_ptr       = hcloud_server.remoteproxy[count.index].name
}

resource "hcloud_rdns" "remoteproxy_floating_rdns_ipv4" {
  count          = var.remoteproxy_count
  floating_ip_id = hcloud_floating_ip.remoteproxy_floating_ipv4_1[count.index].id
  ip_address     = hcloud_floating_ip.remoteproxy_floating_ipv4_1[count.index].ip_address
  dns_ptr        = "home.sirjavik.de"
}

resource "hcloud_rdns" "remoteproxy_floating_rdns_ipv6" {
  count          = var.remoteproxy_count
  floating_ip_id = hcloud_floating_ip.remoteproxy_floating_ipv6_1[count.index].id
  ip_address     = hcloud_floating_ip.remoteproxy_floating_ipv6_1[count.index].ip_address
  dns_ptr        = "home.sirjavik.de"
}

## Domains

resource "cloudflare_record" "sirjavik_de_dns_ipv4" {
  count           = var.remoteproxy_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "sirjavik.de"
  value           = hcloud_primary_ip.remoteproxy_primary_ipv4[count.index].ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}

resource "cloudflare_record" "sirjavik_de_dns_ipv6" {
  count           = var.remoteproxy_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "sirjavik.de"
  value           = "${hcloud_primary_ip.remoteproxy_primary_ipv6[count.index].ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}

resource "cloudflare_record" "sirjavik_de_wilddns_ipv4" {
  count           = var.remoteproxy_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "*.sirjavik.de"
  value           = hcloud_primary_ip.remoteproxy_primary_ipv4[count.index].ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}

resource "cloudflare_record" "sirjavik_de_wilddns_ipv6" {
  count           = var.remoteproxy_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "*.sirjavik.de"
  value           = "${hcloud_primary_ip.remoteproxy_primary_ipv6[count.index].ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}

resource "cloudflare_record" "www_sirjavik_de_dns_ipv4" {
  count           = var.remoteproxy_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "www.sirjavik.de"
  value           = hcloud_primary_ip.remoteproxy_primary_ipv4[count.index].ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}

resource "cloudflare_record" "www_sirjavik_de_dns_ipv6" {
  count           = var.remoteproxy_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "www.sirjavik.de"
  value           = "${hcloud_primary_ip.remoteproxy_primary_ipv6[count.index].ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}
