######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

####
## Javik Remote Access Proxy
####

resource "hcloud_primary_ip" "remoteproxy_primary_ipv4" {
  count = var.remoteproxy_count

  name = format("%s-%s.%s-%s",
    "remote-proxy${count.index + 1}",
    (count.index % 2 == 0 ? "fsn1" : "nbg1"),
    "infra.sirjavik.de",
    "ipv4"
  )

  datacenter    = (count.index % 2 == 0 ? "fsn1-dc14" : "nbg1-dc3")
  type          = "ipv4"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv4",
    terraform = true
  }
}

resource "hcloud_primary_ip" "remoteproxy_primary_ipv6" {
  count = var.remoteproxy_count

  name = format("%s-%s.%s-%s",
    "remote-proxy${count.index + 1}",
    (count.index % 2 == 0 ? "fsn1" : "nbg1"),
    "infra.sirjavik.de",
    "ipv6"
  )

  datacenter    = (count.index % 2 == 0 ? "fsn1-dc14" : "nbg1-dc3")
  type          = "ipv6"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv6",
    terraform = true
  }
}

resource "hcloud_floating_ip" "remoteproxy_floating_ipv4_1" {
  count = var.remoteproxy_count
  name = format("%s-%s.%s-%s",
    "remote-proxy${count.index + 1}",
    (count.index % 2 == 0 ? "fsn1" : "nbg1"),
    "infra.sirjavik.de",
    "ipv4-floating"
  )

  home_location = (count.index % 2 == 0 ? "fsn1" : "nbg1")
  type          = "ipv4"
}

resource "hcloud_floating_ip" "remoteproxy_floating_ipv6_1" {
  count = var.remoteproxy_count
  name = format("%s-%s.%s-%s",
    "remote-proxy${count.index + 1}",
    (count.index % 2 == 0 ? "fsn1" : "nbg1"),
    "infra.sirjavik.de",
    "ipv6-floating"
  )

  home_location = (count.index % 2 == 0 ? "fsn1" : "nbg1")
  type          = "ipv6"
}

resource "hcloud_floating_ip_assignment" "remoteproxy_floating_ipv4_1_assign" {
  count          = var.remoteproxy_count
  floating_ip_id = hcloud_floating_ip.remoteproxy_floating_ipv4_1[count.index].id
  server_id      = hcloud_server.remoteproxy[count.index].id
}

resource "hcloud_floating_ip_assignment" "remoteproxy_floating_ipv6_1_assign" {
  count          = var.remoteproxy_count
  floating_ip_id = hcloud_floating_ip.remoteproxy_floating_ipv6_1[count.index].id
  server_id      = hcloud_server.remoteproxy[count.index].id
}

